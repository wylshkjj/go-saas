package utils

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"os"
)

var (
	AppMode  string
	HttpPort string
	JwtKey   string

	Db         string
	DbHost     string
	DbPort     string
	DbUser     string
	DbPassWord string
	DbName     string

	Rdb         string
	RdbHost     string
	RdbPort     string
	RdbUser     string
	RdbPassWord string
	RdbName     int
	RdbPoolSize int

	TENCENTCLOUD_SECRET_ID  string
	TENCENTCLOUD_SECRET_KEY string
	SdkAppId                string
	SignName                string
	SmsId                   string
)

func init() {
	viper.SetConfigFile("./config/config.yaml") // 指定配置文件路径
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("配置文件读取错误: %s \n", err)
		os.Exit(1)
	}
	Load()
	// 监控配置文件变化
	viper.WatchConfig()
	// 注意！！！配置文件发生变化后要同步到全局变量Conf
	viper.OnConfigChange(func(e fsnotify.Event) {
		// 配置文件发生变更之后会调用的回调函数
		fmt.Println("配置文件已被修改:", e.Name)
		Load()
	})
}

func Load() {
	// server
	AppMode = viper.GetString("server.mode")
	HttpPort = viper.GetString("server.port")
	JwtKey = viper.GetString("server.jwt_key")
	// mysql
	Db = viper.GetString("mysql.db")
	DbHost = viper.GetString("mysql.host")
	DbPort = viper.GetString("mysql.port")
	DbUser = viper.GetString("mysql.user")
	DbPassWord = viper.GetString("mysql.password")
	DbName = viper.GetString("mysql.db_name")
	// redis
	Rdb = viper.GetString("redis.db")
	RdbHost = viper.GetString("redis.host")
	RdbPort = viper.GetString("redis.port")
	RdbUser = viper.GetString("redis.user")
	RdbPassWord = viper.GetString("redis.password")
	RdbName = viper.GetInt("redis.db_name")
	RdbPoolSize = viper.GetInt("redis.pool_size")
	// tencent
	TENCENTCLOUD_SECRET_ID = viper.GetString("tencent.TENCENTCLOUD_SECRET_ID")
	TENCENTCLOUD_SECRET_KEY = viper.GetString("tencent.TENCENTCLOUD_SECRET_KEY")
	SdkAppId = viper.GetString("tencent.SdkAppId")
	SignName = viper.GetString("tencent.SignName")
	SmsId = viper.GetString("tencent.SmsId")
}
